# malscrape

![](https://img.shields.io/badge/written%20in-PHP-blue)

A scraper for the website myanimelist.net.

Also includes a small mobile interface to browse the resulting sqlite database, aimed at user-agents with poor javascript support.

Tags: anime, scraper


## Download

- [⬇️ malscrape-1.0a.7z](dist-archive/malscrape-1.0a.7z) *(24.47 KiB)*
